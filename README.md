# Geração de Assinatura Médica

Este é exemplo um node de integração dos serviços da API de assinatura com clientes baseados em tecnologia Node.js.

Este exemplo apresenta os passos necessários para a geração de metadados referentes ao médico na prescrição médica.

  - Passo 1: Criação do formulário com o documento que será assinado.
  - Passo 2: Recebimento do documento com os metadados.
  - Passo 3: O documento recebido deverá ser enviado para uma assinatura do tipo PDF/PADES.

### Assinatura PDF

Após ter os metadados inseridos, o documento deverá ser enviado novamente ao HUB, para que seja realizada de fato a assinatura. Um Exemplo de como realizar a assinatura se encontra [aqui](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/assinatura-pdf-com-bry-extension/servidor).

### Tech

O exemplo utiliza das bibliotecas Node abaixo:
* [Node] - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine.
* [Axios] - Promise based HTTP client for the browser and node.js

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| token | Access Token para o consumo do serviço (JWT). |     src/index.js    | 

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

### Uso

É necessário ter o [Node] instalado na sua máquina para a instalação das dependências e execução do projeto.

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência e instale as dependências. Utilizamos o Node versão 12 e yarn (ou npm) para a instalação das dependências.

Comandos:

Instalar dependências:

    -yarn
    
ou
    
    -npm install

Executar programa:

    -node src/index.js

  [Node]: <https://nodejs.org/en/>
  [Axios]: <https://github.com/axios/axios>

