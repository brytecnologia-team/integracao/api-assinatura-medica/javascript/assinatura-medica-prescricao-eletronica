const Formdata = require('form-data');
const axios = require('axios').default;
const fs = require('fs');
const path = require('path');


async function adicionaMetadados() {

    // Token de autenticação gerado no BRy Cloud
    const token = "tokenDeAutenticação";
    // Cria formulário que será enviado na requisição para inserção de MetaDados
    const form = new Formdata();

    // Documento 1 que irá ser assinado
    form.append(
        'prescricao[0][documento]', 
        fs.createReadStream(path.resolve(__dirname, '..', 'arquivos', 'teste.pdf'))
        );
    // Tipo de prescrição. [Valores disponíveis: MEDICAMENTO, ATESTADO, SOLICITACAO_EXAME,
    // LAUDO, SUMARIA_ALTA, ATENDIMENTO_CLINICO, DISPENSACAO_MEDICAMENTO, VACINACAO e 
    // RELATORIO_MEDICO].
    form.append('prescricao[0][tipo]', 'MEDICAMENTO');
    // Tipo de profissional. [Valores disponíveis: MEDICO ou FARMACEUTICO]
    form.append('profissional', 'MEDICO');
    // Número de registro do profissional
    form.append('numeroRegistro', '123456');
    // Sigla do estado em que o número de registro do profissional está cadastrado.
    form.append('UF', 'SC');
    // Especialidade do profissional
    form.append('especialidade', 'ONCOLOGISTA');

    const header = {
          "Authorization": token,
          'Content-Type': `multipart/form-data; boundary=${form.getBoundary()}`,
        }
    
    try {
        const response = await axios.post('https://hub2.dev.bry.com.br/pdf/v1/prescricao', form, {headers: header, responseType: 'arraybuffer'});
        // O retorno será o arquivo com os MetaDados relacionados à prescrição, que agora precisará ser assinado.
        fs.writeFile(path.resolve(__dirname, '..', 'arquivos', 'arquivoComMetadados.pdf'), response.data, { encoding: "binary" }, function(err) {
            console.log('File created');
        });
    } catch (err) {
        console.log(err);
    }
}

adicionaMetadados();
